##site.pp ##

# Define filebucket 'main':
filebucket { 'main':
  server => 'negril.punchtech.com',
  path   => false,
}

# Make filebucket 'main' the default backup location for all File resources:
File { backup => 'main' }

node default {
  notify { "Running in ${environment} environment": }
}

